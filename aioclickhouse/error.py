__all__ = ['DatabaseException']


class DatabaseException(BaseException):
    pass
