import asyncio
import unittest
from unittest.mock import Mock
from aioclickhouse.connection import Connection
from aioclickhouse.error import DatabaseException


class ConnectionTestCase(unittest.TestCase):
    def setUp(self):
        self.conn = Connection('localhost', 8123)

    def test_connection_url(self):
        self.assertEqual(self.conn._url, 'http://localhost:8123/?database=default')

    def test_execute_result(self):
        async def test():
            async def text():
                return '1\t2\t3'

            async def post(url, data):
                self.assertEqual(data, 'test')

                resp = Mock()
                resp.status = 200
                resp.text = text
                return resp

            self.conn.session = Mock()
            self.conn.session.post = post

            res = await self.conn.execute('test')
            self.assertListEqual(list(res), [['1', '2', '3']])

        asyncio.get_event_loop().run_until_complete(test())

    def test_execute_error(self):
        async def test():
            async def text():
                return 'error text'

            async def post(url, data):
                self.assertEqual(data, 'test')

                resp = Mock()
                resp.status = 500
                resp.text = text
                return resp

            self.conn.session = Mock()
            self.conn.session.post = post

            with self.assertRaises(DatabaseException) as exp:
                await self.conn.execute('test')

            self.assertEqual(str(exp.exception), 'error text')

        asyncio.get_event_loop().run_until_complete(test())

if __name__ == '__main__':
    unittest.main()
